#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <chrono>
#include <boost/algorithm/string.hpp>
#include <unordered_set>

using namespace std;

template <typename UnorderedCont>
void show_stat(const UnorderedCont& cont, const string& prefix)
{
    cout << prefix << ": size: " << cont.size()
         << "; load_factor: " << cont.load_factor()
         << "; bucket_count: " << cont.bucket_count()
         << "; max_load_factor: " << cont.max_load_factor() << endl;
}

int main()
{
     // wszytaj zawartość pliku en.dict ("słownik języka angielskiego")
     // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list;
    string input_text{"this is an exmple of very badd snetence"};
    boost::split(words_list, input_text, boost::is_any_of("\t "));

    ifstream fin("../en.dict");

    if (!fin)
    {
        cout << "Blad otwarcia pliku..." << endl;
        exit(1);
    }

    unordered_set<string>
            dict(istream_iterator<string>{fin}, istream_iterator<string>{}, 128000);

//    unordered_set<string> dict(128000);
//    dict.insert(istream_iterator<string>{fin}, istream_iterator<string>{});

    cout << "misspelled words: ";
//    for(const auto& word: words_list)
//        if (!dict.count(word))
//            cout << word << " ";

    copy_if(words_list.cbegin(), words_list.cend(),
            ostream_iterator<string>(cout, " "),
            [&](const string& w){ return !dict.count(w); });
    cout << endl;
}

