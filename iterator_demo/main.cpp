#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <iterator>

using namespace std;

void print(int x)
{
    cout << "item: " << x << endl;
}

template <typename InpIt, typename OutIt>
void mcopy(InpIt start, InpIt end, OutIt out)
{
    while (start != end)
    {
        *out++ = *start++;
    }
}

bool is_even(int x)
{
    return x % 2 == 0;
}

int main()
{
    vector<int> vec1 = { 12, 5, 6, 7, 34, 22, 1 };

    cout << "vec1:\n";
    for_each(vec1.cbegin(), vec1.cend(), &print);

    list<int> lst1;

    //back_insert_iterator<list<int>> bit{lst1};
    mcopy(vec1.cbegin(), vec1.cend(), back_inserter(lst1));

    cout << "\nlst1:\n";
    for_each(lst1.cbegin(), lst1.cend(), &print);

    sort(vec1.begin(), vec1.end());
    cout << "\nvec1:\n";
    for_each(vec1.cbegin(), vec1.cend(), &print);

    mcopy(lst1.cbegin(), lst1.cend(), inserter(vec1, vec1.begin() + 3));

    cout << "\nvec1:\n";
    for_each(vec1.cbegin(), vec1.cend(), &print);

    list<int> evens;
    copy_if(vec1.cbegin(), vec1.cend(), back_inserter(evens), &is_even);
    cout << "\nevens:\n";
    for_each(evens.cbegin(), evens.cend(), &print);

    cout << "\n\nPodaj liczby:\n";

    istream_iterator<int> start {cin};
    istream_iterator<int> end;

    vector<int> numbers(start, end);

    cout << "\nnumbers: ";
    copy(numbers.cbegin(), numbers.cend(), ostream_iterator<int>{cout, " "});
    cout << endl;
}
