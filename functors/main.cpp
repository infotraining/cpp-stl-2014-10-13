#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>

using namespace std;

inline void print(int x)
{
    cout << "item: " << x << endl;
}

class Printer
{
public:
    void operator()(int x) const
    {
        cout << "printing: " << x << endl;
    }
};

class Sum
{
    int result_ = 0;
public:
    void operator()(int x)
    {
        result_ += x;
    }

    int result() const
    {
        return result_;
    }
};

template <typename InIt, typename Function>
Function mfor_each(InIt start, InIt end, Function func)
{
    while (start != end)
    {
        func(*(start++));  //func.operator()(x) moze byc inline
    }

    return func;
}

bool is_leap(unsigned int year)
{
    if (year % 400 == 0) return true;
    if (year % 100 == 0) return false;
    if (year % 4 == 0) return true;
    return false;
}

struct IsLeap //: public unary_function<unsigned int, bool>
{
    typedef unsigned int argument_type;
    typedef bool result_type;

    bool operator()(unsigned int year) const
    {
        if (year % 400 == 0) return true;
        if (year % 100 == 0) return false;
        if (year % 4 == 0) return true;
        return false;
    }
};

int main()
{
    vector<int> vec1 = { 1, 2, 3, 4, 5, 6, 8, -4, 2, 1 };

    mfor_each(vec1.cbegin(), vec1.cend(), &print);
    // t<InIt = vector<int>::const_iterator, Function = void(*)(int)> for_each(...)

    cout << "\n\n";

    Printer prn;

    mfor_each(vec1.begin(), vec1.end(), prn);
    // t<InIt = vector<int>::iterator, Function = Printer> for_each(...)

    cout << "\n\n";

    mfor_each(vec1.begin(), vec1.end(), Printer());

    cout << "\n\n";

    auto sum = for_each(vec1.begin(), vec1.end(), Sum());

    cout << "Sum: " << sum.result() << endl;

    cout << "\n\n";

    Sum adder;

    mfor_each(vec1.begin(), vec1.end(), ref(adder));

    cout << "Sum: " << adder.result() << endl;

    cout << "\n\n";

    auto printer = [](int x) { cout << "lambda: " << x << endl; };

    mfor_each(vec1.begin(), vec1.end(), printer);

    cout << "\n\n";

    function<void (int)> f = printer;

    mfor_each(vec1.begin(), vec1.end(), f);

    cout << "\n\n";

    int sum_result = 0;

    mfor_each(vec1.cbegin(), vec1.cend(), [&](int x) { sum_result += x; });

    cout << "Sum: " << sum_result << endl;

    cout << "\n\n";

    auto where = find_if(vec1.begin(), vec1.end(), [](int x) { return x > 5; });

    if (where != vec1.end())
    {
        cout << "Pierwszy element większy od 5: " << *where << endl;
    }

    cout << "\n\n";

    list<int> evens;

    string prefix = "Item: ";

    mfor_each(vec1.begin(), vec1.end(),
              [=, &evens](int x) {
                    cout << prefix << x << endl;
                    if (x % 2 == 0) evens.push_back(x);
              });

    cout << "\n\n";

    mfor_each(evens.begin(), evens.end(), printer);

    cout << "\n\n";

    vector<unsigned int> years = { 1994, 2001, 2005, 1999, 1993, 2012, 1996 };

    auto first_leap = find_if(years.begin(), years.end(), IsLeap());

    if (first_leap != years.end())
        cout << "Pierwszy przestepny rok: " << *first_leap << endl;

    auto first_not_leap = find_if(years.begin(), years.end(), not1(IsLeap()));

    if (first_not_leap != years.end())
        cout << "Pierwszy nieprzestepny rok: " << *first_not_leap << endl;

    cout << "\n\n";

    auto last_leap = find_if(years.rbegin(), years.rend(), &is_leap);

    if (last_leap != years.rend())
        cout << "Ostatatni przestepny rok: " << *last_leap << endl;

    auto last_not_leap = find_if(years.rbegin(), years.rend(), not1(ptr_fun(&is_leap)));

    if (last_not_leap != years.rend())
        cout << "Ostatni nieprzestepny rok: " << *last_not_leap << endl;
}
