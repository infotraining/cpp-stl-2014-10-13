#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>

using namespace std;

void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

int main()
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    cout << "tab: ";
    for(int i = 0; i < 100; ++i)
        cout << tab[i] << " ";
    cout << "\n\n";

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    vector<int> vec_int(begin(tab), end(tab));

    // 2 - wypisz wartość średnią przechowywanych liczb w vec_int
    avg(vec_int.data(), vec_int.size());

//    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
//    auto vec_cloned = vec_int;

//    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
//    vec_int.clear();
//    vec_int.shrink_to_fit();

    // 3a i 3b
    vector<int> vec_cloned = move(vec_int);

    int rest[] = { 1, 2, 3, 4 };
    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest
    vec_cloned.insert(vec_cloned.end(), begin(rest), end(rest));

    // 5 - posortuj zawartość wektora vec_cloned
    sort(vec_cloned.begin(), vec_cloned.end());

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów
    cout << "\nvec_cloned: ";
    for(auto it = vec_cloned.cbegin(); it != vec_cloned.cend(); ++it)
        cout << *it << " ";
    cout << "\n\n";

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia) zawierający kopie elementów vec_cloned
    list<int> numbers(vec_cloned.begin(), vec_cloned.end());

    // 8 - usuń duplikaty elementów przechowywanych w kontenerze
    numbers.unique();

    // 9 - usuń liczby parzyste z kontenera
    numbers.remove_if(&is_even);

    // 10 - wyświetl elementy kontenera
    cout << "numbers: ";
    for(const auto& item : numbers)
        cout << item <<  " ";
    cout << "\n\n";

    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers
    auto lst_numbers = move(numbers);

    // C++03
//    list<int> alt_numbers;
//    alt_numbers.splice(alt_numbers.begin(), lst_numbers);

    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
    cout << "lst_numbers: ";
    for(auto it = lst_numbers.crbegin(); it != lst_numbers.crend(); ++it)
        cout << *it << " ";
    cout << "\n\n";
}

