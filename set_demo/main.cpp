#include <iostream>
#include <set>
#include <tuple>
#include <memory>

using namespace std;

template <typename Container>
void print(Container& cont, const string& prefix)
{
    cout << prefix << " : [ ";
    for(auto it = cont.cbegin(); it != cont.cend(); ++it)
        cout << *it << " ";
    cout << "]\n";
}

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator<(const Person& other) const
    {
        return tie(id_, name_) < tie(other.id_, other.name_);
    }

    friend class ComparePersonByName;
};

class ComparePersonByName
{
public:
    bool operator()(const Person& p1, const Person& p2) const
    {
        return tie(p1.name_, p1.id_) < tie(p2.name_, p2.id_);
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id=" << p.id() << ", name=" << p.name() << ")";
    return out;
}

int main()
{
    set<int> set1;

    set1.insert(5);
    set1.insert({2, 6, 7, 0, 9, 5, 1});

    print(set1, "set1");

    set<int>::iterator where;
    bool insert_perfomed;

    tie(where, insert_perfomed) = set1.insert(4);

    if (insert_perfomed)
        cout << "Wartosc " << *where << " została wstawiona" << endl;
    else
        cout << "Wartosc " << *where << " byla juz w zbiorze" << endl;

    print(set1, "set1");

    tie(where, ignore) = set1.insert(10);

    cout << "*where = " << *where << endl;

    where = set1.find(5);

    if (where != set1.end())
        cout << "Znalazlem " << *where << endl;

    cout << "\n\n";

    multiset<int> mset1 { set1.begin(), set1.end() };

    mset1.insert({ 5, 6, 5, 6, 5 });

    print(mset1, "mset1");

    auto start = mset1.lower_bound(5);
    auto end = mset1.upper_bound(5);

    multiset<int> fives { start, end };

    print(fives, "fives");

    tie(start, end) = mset1.equal_range(3);

    if (start == end)
    {
        cout << "3 nie znajduje sie w zbiorze" << endl;
        mset1.insert(start, -1);
        cout << "wstawiono 3" << endl;
    }

    print(mset1, "mset1");

    set<Person, ComparePersonByName> people;

    people.insert(Person{1, "Kowalski"});
    people.emplace(7, "Nowak");
    people.insert(Person {2, "Anonim"});
    people.insert(Person{9, "Nijaki"});

    print(people, "people");

    auto compare_ptrs =
            [](const shared_ptr<int>& ptr1, const shared_ptr<int>& ptr2) { return *ptr1 < *ptr2; };
    //auto compare_ptrs = [](auto& ptr1, auto& ptr2) { return *ptr1 < *ptr2; }; // auto

    set<shared_ptr<int>, decltype(compare_ptrs)> ptrs{compare_ptrs};

    ptrs.insert({ make_shared<int>(7), make_shared<int>(2), make_shared<int>(9) });

    ptrs.emplace(new int{4});

    cout << "ptrs: ";
    for(auto& ptr : ptrs)
    {
        cout << *ptr <<  " ";
    }
    cout << endl;
}
