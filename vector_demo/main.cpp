#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>

using namespace std;

template <typename T>
void show_stat(const vector<T>& vec, const string& text = "")
{
    cout << text << " -  size: " << vec.size()
         << "; capacity = " << vec.capacity() << endl;
}

template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;

//    for(decl : container)
//        statement;

// 1.
//    for(auto it = container.begin(); it != container.end(); ++it)
//    {
//        decl item = *it;

//        statement;
//    }

// 2
//    for(auto it = begin(container); it != end(container); ++it)
//    {
//        decl item = *it;

//        statement;
//    }
}

class Person
{
    int id_;
    string name_ = "unknown";
    static int get_id()
    {
        static int id;
        return ++id;
    }

public:
    Person() : id_ {get_id()}
    {
        cout << "Person(id = " << id_ << ")"<< endl;
    }

    Person(int id, const string& name) : id_ {id}, name_ {name}
    {
        cout << "Person(id = " << id_ << ", name=" << name_ <<  ")"<< endl;
    }

    Person(const Person& source) : id_{source.id()}, name_{source.name()}
    {
        cout << "Person(const Person&: id = " << id_ << ", name_ " << name_ << ")" << endl;
    }

    Person& operator=(const Person& source)
    {
        if (this != &source)
        {
            id_ = source.id_;
            name_ = source.name_;
        }

        cout << "operator=(const Person&: id = " << id_ << ", name_ " << name_ << ")" << endl;

        return *this;
    }

    Person(Person&& source) noexcept : id_ { move(source.id_) }, name_ { move(source.name_) }
    {
        cout << "Person(Person&&: id = " << id_ << ", name_ " << name_ << ")" << endl;
    }

    Person& operator=(Person&& source)
    {
        if (this != &source)
        {
            id_ = move(source.id_);
            name_ = move(source.name_);
        }

        cout << "operator=(Person&&: id = " << id_ << ", name_ " << name_ << ")" << endl;

        return *this;
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id=" << p.id() << ", name='" << p.name() << "')";
    return out;
}

Person create_person(int id, const string& name)
{
    return Person{id, name};
}

int main()
{
    vector<int> vec1;

    cout << vec1.data() << endl;

    show_stat(vec1, "vec1");

    // vec1[0] = 3; // undefined behavior

    vector<int> vec2(10);

    show_stat(vec2, "vec2");

    print(vec2, "vec2");

    int tab1[] = { 1, 2, 3, 4 };

    print(tab1, "tab1");

    auto il = { 1, 2, 3, 4 };

    // print({1, 2, 3, 4}, "il"); // compilation error
    print(il, "il");

    vector<int> vec3(10, -1);

    print(vec3, "vec3");

    vector<Person> people1(10);

    print(people1, "people1");

    vector<int> vec4(begin(tab1), end(tab1));

    print(vec4, "vec4");

    vector<int> vec5(vec4.begin(), vec4.end());

    print(vec5, "vec5");

    //vector<int> vec6 = { 1, 2, 3, 4, 5, 6 };
    vector<int> vec6 { 1, 2, 3, 4, 5, 6 };
    print(vec6, "vec6");

    vec6.insert(vec6.begin() + 3, -1);
    print(vec6, "vec6");

    vec6.insert(vec6.end(), { 1, 2, 3, 4, 5, 6 });

    print(vec6, "vec6");


    vector<int> vec7 {12, -1};

    print(vec7, "vec7");

    vector<string> words {3, "unkown"};

    print(words, "words");

    auto size = vec4.max_size();
    cout << "max_size <int>: " << vec4.max_size() << endl;
    cout << "max_size: <string>: " << words.max_size() << endl;

    vector<int> numbers = { 1, 8, 34, 66, 2, 4, 6, 8, 21, 33, 55 };

    for(auto it = numbers.cbegin(); it != numbers.cend(); ++it)
        cout << "item: " << *it << ", ";
    cout << endl;

    for(auto it = numbers.crbegin(); it != numbers.crend(); ++it)
        cout << "item: " << *it << ", ";
    cout << endl;

    auto where_first = find(numbers.begin(), numbers.end(), 2);

    if (where_first != numbers.end())
        cout << "Znalazlem " << *where_first << " na indeksie: " << (where_first - numbers.begin()) << endl;

    vector<int>::reverse_iterator rit(where_first);
    cout << "*rit: " << *rit << endl;
    ++rit;
    cout << "*rit po ++: " << *rit << endl;

    auto where_last = find(numbers.rbegin(), numbers.rend(), 2);

    if (where_last != numbers.rend())
    {
        cout << "Znalazlem " << *where_last << " na indeksie: " << (where_last.base()-1 - numbers.begin()) << endl;
    }

    vector<int> vec8;
    vec8.reserve(100);

    for(int i = 0; i < 100; ++i)
    {
        vec8.push_back(i);
        show_stat(vec8, "vec8");
    }

    vec8.resize(10);
    vec8.reserve(0);
    //vec8.shrink_to_fit();

    vector<int>(vec8).swap(vec8); // C++03

    vec8.push_back(9);

    show_stat(vec8, "vec8 po clear");


    Person p1 {1, "Kowalski"};
    Person p2(2, "Nowak");

    people1.push_back(p1); // push_back przez kopiowanie - push_back(const T&)
    people1.push_back(move(p2)); // push_back przez move - push_back(T&&)

    cout << "people1.back() - " << people1.back() << endl;
    cout << "p2 = " << p2 << endl;

    people1.push_back(people1[0]);
    people1.push_back(create_person(6, "Zet"));
    people1.push_back(Person{3, "Anonim"}); // push_back przez move - push_back(T&&)
    people1.emplace_back(4, "Nijaki");

    vector<unique_ptr<Person>> people_ptrs;

    unique_ptr<Person> ptr1 { new Person{1, "Nowak"} };

    people_ptrs.push_back(move(ptr1));
    people_ptrs.push_back(unique_ptr<Person>{new Person{3, "Kowalski"}});
    people_ptrs.emplace_back(new Person{4, "Nijaki"});

    for(const auto& ptr : people_ptrs)
        cout << ptr->name() << endl;

    cout << "\n\n-----------------------" << endl;

    vector<Person> test_ms;

    test_ms.push_back(Person {1, "Kowalski"});
    cout << "\n";
    test_ms.push_back(Person {2, "Nowak"});
}








