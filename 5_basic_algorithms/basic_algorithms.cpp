#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    std::random_device rd;
    std::mt19937 mt {rd()};
    std::uniform_int_distribution<int> uniform_dist {1, 30};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    auto is_even = [](int x) { return x % 2 == 0;};

    // 1a - wyświetl parzyste
    cout << "Parzyste: ";
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "), is_even);
    cout << endl;

    // 1b - wyswietl ile jest nieparzystych
    cout << "Ilosc nieparzystych: "
         << count_if(vec.begin(), vec.end(), [](int x) { return x % 2; }) << endl;

    // 1c - wyswietl ile jest parzystych
    cout << "Ilosc parzystych: "
         << count_if(vec.begin(), vec.end(), is_even) << endl;

    int eliminators[] = { 3, 5, 7 };
    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    auto garbage_start =
            remove_if(vec.begin(), vec.end(),
                      [&eliminators](int x) { return any_of(begin(eliminators), end(eliminators),
                                              [x](int y) { return x % y == 0; });});

    vec.erase(garbage_start, vec.end());

    cout << "Po usnięciu: ";
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    // 3 - tranformacja: podnieś liczby do kwadratu
    //transform(vec.begin(),vec.end(), vec.begin(), [](int x) { return x * x;});
    transform(vec.begin(), vec.end(), vec.begin(), vec.begin(), multiplies<int>());

    cout << "Kwadraty: ";
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    // 4 - wypisz 5 najwiekszych liczb
    cout << "5 największych: ";
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
    cout << endl;

    // 5 - policz wartosc srednia
    double sum {0.0};

//    for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x;});
//    double avg = sum / vec.size();

    double avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

    cout << "Avg: " << avg << endl;

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    vector<int> gt_eq_than_avg;
    vector<int> less_than_avg;

    partition_copy(vec.begin(), vec.end(), back_inserter(gt_eq_than_avg), back_inserter(less_than_avg),
                   [avg](int x) { return x >= avg; });

    cout << "gt_eq_than_avg: ";
    copy(gt_eq_than_avg.begin(), gt_eq_than_avg.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    cout << "less_than_avg: ";
    copy(less_than_avg.begin(), less_than_avg.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
}
