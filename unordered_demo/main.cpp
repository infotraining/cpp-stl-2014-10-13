#include <iostream>
#include <unordered_set>
#include <functional>
#include <tuple>
#include <boost/functional/hash.hpp>

using namespace std;

template <typename UnorderedCont>
void show_stat(const UnorderedCont& cont, const string& prefix)
{
    cout << prefix << ": size: " << cont.size()
         << "; load_factor: " << cont.load_factor()
         << "; bucket_count: " << cont.bucket_count()
         << "; max_load_factor: " << cont.max_load_factor() << endl;
}

template <typename UnorderedCont>
void show_full_stat(const UnorderedCont& cont, const string& prefix)
{
    cout << prefix << ": size: " << cont.size()
         << "; load_factor: " << cont.load_factor()
         << "; bucket_count: " << cont.bucket_count()
         << "; max_load_factor: " << cont.max_load_factor() << endl;

    for(auto i = 0; i < cont.bucket_count(); ++i)
    {
        cout << "bucket[" << i << "] = " << cont.bucket_size(i) << endl;
    }
}

struct Point
{
    int x, y;

    Point(int x, int y) : x(x), y(y)
    {}

    bool operator==(const Point& pt) const
    {
        return tie(x, y) == tie(pt.x, pt.y);
    }
};

namespace std
{
    template <>
    struct hash<Point>
    {
        size_t operator()(const Point& pt) const
        {
            size_t seed = 0;

            boost::hash_combine(seed, pt.x);
            boost::hash_combine(seed, pt.y);

            return seed;
        }
    };
}

struct PointHash
{
    size_t operator()(const Point& pt) const
    {
        size_t seed = 0;

        boost::hash_combine(seed, pt.x);
        boost::hash_combine(seed, pt.y);

        return seed;
    }
};

int main()
{
    unordered_set<int> uset1;

    uset1.max_load_factor(2);

    show_stat(uset1, "uset1");

    auto prev_bucket_count = uset1.bucket_count();

    for(int i = 0; i < 1000; ++i)
    {
        uset1.insert(rand());
        if (prev_bucket_count != uset1.bucket_count())
        {
            cout << "Rehash: ";
            show_stat(uset1, "uset1");
            prev_bucket_count = uset1.bucket_count();
        }
    }

    show_stat(uset1, "uset1");

    uset1.rehash(5000);

    show_stat(uset1, "uset1");

    cout << "\n\n";

    unordered_set<Point, PointHash> points;

    for(int i = 0; i < 1000; ++i)
        points.insert(Point{rand(), rand()});

    show_stat(points, "points");

    auto where = points.find(Point {5, 123});

    if (where != points.end())
        cout << "Punkt jest w kontenerze..." << endl;
    else
        cout << "Brak punktu w kontenerze..." << endl;

    cout << "\n\n";

    show_full_stat(points, "points");
}
