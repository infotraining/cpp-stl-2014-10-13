#include <vector>
#include <random>
#include <set>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <tuple>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix)
{
    cout << prefix << " : [ ";
    for(const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    vector<int> vec(35);

    std::random_device rd;
    std::mt19937 mt {rd()};
    std::uniform_int_distribution<int> uniform_dist {1, 35};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    vector<int> sorted_vec {vec};
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset {vec.begin(), vec.end()};

    print(vec, "vec: ");
    print(sorted_vec, "sorted_vec: ");
    print(mset, "mset: ");

    // czy 17 znajduje sie w sekwencji?
    cout << (find(vec.begin(), vec.end(), 17) != vec.end()) << endl;
    cout << binary_search(sorted_vec.begin(), sorted_vec.end(), 17) << endl;
    cout << (mset.find(17) != mset.end()) << endl;

    cout << "\n";

    // ile razy wystepuje 22?
    cout << count(vec.begin(), vec.end(), 22)  << endl;

    vector<int>::iterator start, end;
    tie(start, end) = equal_range(sorted_vec.begin(), sorted_vec.end(), 22);
    cout << (end - start) << endl;

    cout << mset.count(22)  << endl;

    // usun wszystkie wieksze od 15
    vec.erase(remove_if(vec.begin(), vec.end(), [](int x) { return x > 15;}), vec.end());
    sorted_vec.erase(upper_bound(sorted_vec.begin(), sorted_vec.end(), 15), sorted_vec.end());
    mset.erase(mset.upper_bound(15), mset.end());
}
