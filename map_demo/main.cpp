#include <iostream>
#include <map>

using namespace std;

template <typename Container>
void print(Container& cont, const string& prefix)
{
    cout << prefix << " : [ ";
    for(auto it = cont.cbegin(); it != cont.cend(); ++it)
        cout << *it << " ";
    cout << "]\n";
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "(" << p.first << ", " << p.second << ")";
    return out;
}

template <typename Value>
using InversedDictionary = map<string, Value>;

int main()
{
    using Dictionary = multimap<int, string, greater<int>>;

    Dictionary dict = { {1, "one"}, {3, "three"}, {2, "two"} };

    dict.insert(Dictionary::value_type {7, "seven"});
    dict.insert(pair<int, string> { 9, "nine"});
    dict.insert(make_pair(4, "four"));

    dict.insert(make_pair(9, "dziewiec"));
    print(dict, "dict");

    cout << "9 : " << dict.find(9)->second << endl;

    Dictionary::iterator start, end;
    tie(start, end) = dict.equal_range(9);

    cout << "9 : ";
    for(auto it = start; it != end; ++it)
        cout << it->second << " ";
    cout << endl;

    print(dict, "dict");

    InversedDictionary<int> inversed_dict;

    for(auto& kv : dict)
    {
        inversed_dict.insert(make_pair(kv.second, kv.first));
    }

    cout << "nine: " << inversed_dict["nine"] << endl;
    //cout << "8: " << inversed_dict[9] << endl; // error
}
