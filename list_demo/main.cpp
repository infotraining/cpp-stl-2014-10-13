#include <iostream>
#include <list>
#include <algorithm>
#include <vector>
#include <deque>

using namespace std;

template <typename Container>
void print(const Container& container, const string& name)
{
    cout << name << ": [ ";
    for(const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    list<int> lst1 = { 2, 7, 6, 4, 5, 7, 12, -1, 7 };

    print(lst1, "lst1");

    list<int> lst2 = { 7, 8, 9, 0, 3, 2, 2 };

    print(lst2, "lst2");

    auto where = find(lst1.begin(), lst1.end(), 5);

    auto it2 = lst2.begin();
    advance(it2, 3);

    lst1.splice(where, lst2, lst2.begin(), it2 );

    cout << "\n\n";

    print(lst1, "lst1");
    print(lst2, "lst2");

    cout << "\n\n";

    lst1.sort();
    lst2.sort();

    lst1.merge(lst2);

    print(lst1, "lst1");
    print(lst2, "lst2");

    cout << "\n\n";

    deque<string> coll = { "one", "three", "four", "five" };

    auto it = coll.begin();
    advance(it , 3);

    string* ptr = &(*it);
    string& ref = *it;

    cout << "*it = " << *it << "; *ptr = " << *ptr << "; ref = " << ref << endl;

    coll.push_back("nine");

    cout << "*it = " << *it << "; *ptr = " << *ptr << "; ref = " << ref << endl;
}
