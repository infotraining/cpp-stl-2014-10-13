#include <map>
#include <iostream>
#include <fstream>
#include <iterator>
#include <set>
#include <string>
#include <boost/tokenizer.hpp>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym.
    Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

using WordsContainer = vector<string>;

WordsContainer load_words_from_file(const string& file_name)
{
    ifstream file_book {file_name};

    if (!file_book)
    {
        cout << "Blad otwarcia pliku!!!" << endl;
        exit(1);
    }

    string line;
    WordsContainer words;

    while(getline(file_book, line))
    {
        boost::tokenizer<> tok {line};

        words.insert(words.end(), tok.begin(), tok.end());
    }

    return words;
}

int main()
{
    auto words = load_words_from_file("../alice.txt");

    using Concordance = map<string, int>;

    Concordance concordance;

    for(const auto& word : words)
        concordance[word]++;

// ver 1.0

//    multimap<int, string, greater<int>> words_by_freq;

//    for(const auto& kv : concordance)
//        words_by_freq.insert(make_pair(kv.second, kv.first));

//    int counter = 0;
//    for(auto it = words_by_freq.cbegin(); it != words_by_freq.end(); ++it)
//    {
//        cout << it->second << " - " << it->first << endl;
//        if (++counter >= 10)
//            break;
//    }

// ver 1.1

    using ConordanceIter = Concordance::iterator;

    vector<ConordanceIter> words_by_freq;
    words_by_freq.reserve(concordance.size());

    for(auto it = concordance.begin(); it != concordance.end(); ++it)
        words_by_freq.push_back(it);

    partial_sort(words_by_freq.begin(), words_by_freq.begin() + 10, words_by_freq.end(),
                 [](ConordanceIter it1, ConordanceIter it2) { return (*it1).second > (*it2).second; });

    int counter = 0;
    for(auto it = words_by_freq.cbegin(); it != words_by_freq.begin() + 10; ++it)
        cout << (*it)->first << " - " << (*it)->second << endl;
}
