#include <iostream>
#include <array>

using namespace std;

template <typename Container>
void print(Container& cont, const string& prefix)
{
    cout << prefix << " : [ ";
    for(auto it = cont.cbegin(); it != cont.cend(); ++it)
        cout << *it << " ";
    cout << "]\n";
}

void legacy_code(int tab[], size_t size)
{
    for(int* ptr = tab; ptr != tab + size; ++ptr)
        cout << *ptr << " ";
    cout << "\n";
}

int main()
{
    int tab1[10];

    array<int, 10> arr1 = { 1, 2, 4, 5, 5, 8, 9, -1};

    print(arr1, "arr1");

    decltype(arr1) arr2 = {};

    print(arr2, "arr2");

    legacy_code(arr2.data(), arr2.size());

    arr1.swap(arr2);

    cout << "po swap: " << endl;

    print(arr1, "arr1");
    print(arr2, "arr2");
}
