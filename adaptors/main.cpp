#include <iostream>
#include <vector>
#include <string>
#include <functional>
#include <memory>
#include <algorithm>
#include <iterator>

using namespace std;

class Person
{
    int id_;
    string name_;
    unsigned int age_;
public:
    Person(int id, const string& name, unsigned int age)
        : id_{id}, name_{name}, age_{age}
    {}

    int id() const
    {
        return id_;
    }

    unsigned int age() const
    {
        return age_;
    }

    string name() const
    {
        return name_;
    }

    void info() const
    {
        cout << "Osoba: " << id_ << " - " << name_ << "- age: " << age_ << endl;
    }

    bool is_retired()
    {
        return age_ > 67;
    }
};

using Callback = function<void (string)>;

class Device
{

    vector<Callback> callbacks_;

public:
    void register_callback(Callback c)
    {
        callbacks_.push_back(c);
    }

    void run()
    {
        for(auto& c : callbacks_)
            c("run");
    }
};

void mylog(string msg)
{
    cout << "Log: " << msg;
}

class Logger
{
public:
    void info(string msg) { cout << "Logging info: " << msg; }
};

int main()
{
    vector<Person> people = { Person{1, "Nowak", 45}, Person{3, "Kowalski", 71},
                              Person{2, "Anonim", 23}, Person{7, "Nijaki", 69} };


    vector<Person*> people_ptrs = { new Person{1, "Nowak", 45}, new Person{3, "Kowalski", 71},
                                   new Person{2, "Anonim", 23}, new Person{7, "Nijaki", 69} };

    vector<shared_ptr<Person>> people_sptrs = { make_shared<Person>(1, "Nowak", 45),
                                          make_shared<Person>(3, "Kowalski", 71),
                                          make_shared<Person>(2, "Anonim", 23),
                                          make_shared<Person>(7, "Nijaki", 69) };

    // C++03
    for_each(people.begin(), people.end(), mem_fun_ref(&Person::info));

    cout << "\n";

    for_each(people_ptrs.begin(), people_ptrs.end(), mem_fun(&Person::info));


    // C++11

    for_each(people.begin(), people.end(), mem_fn(&Person::info));

    cout << "\n";

    for_each(people_ptrs.begin(), people_ptrs.end(), mem_fn(&Person::info));

    cout << "\n";

    for_each(people_sptrs.begin(), people_sptrs.end(), mem_fn(&Person::info));

    cout << "\n";

    for_each(people_sptrs.begin(), people_sptrs.end(),
             [](const shared_ptr<Person>& ptr) { ptr->info(); });

    cout << "\n";

    for(auto& sptr : people_sptrs)
        sptr->info();

    vector<shared_ptr<Person>> emps;
    vector<shared_ptr<Person>> retired;

    int threshold = 65;

    partition_copy(people_sptrs.begin(), people_sptrs.end(),
                   back_inserter(retired), back_inserter(emps),
                   [=] (const shared_ptr<Person>& ptr) { return ptr->age() > threshold; });

//    copy_if(people_sptrs.begin(), people_sptrs.end(), back_inserter(retired),
//            //mem_fn(&Person::is_retired));
//            [=] (const shared_ptr<Person>& ptr) { return ptr->age() > threshold; });


    cout << "\nretired:\n";
    for_each(retired.begin(), retired.end(), mem_fn(&Person::info));

    cout << "\nemps:\n";
    for_each(emps.begin(), emps.end(), mem_fn(&Person::info));

    cout << "\n\n";

//    Device dev;
//    Logger logger;

//    dev.register_callback(&mylog);
//    dev.register_callback([&logger](string msg) { logger.info(msg); });

//    dev.run();

    vector<int> vec = { 5, 7, 5, 2, 1, 5, 6, 7, 3, 6, 7, 2, 5, 7, 3, 4 };

    cout << "vec przed remove: ";
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    auto where = remove(vec.begin(), vec.end(), 7);

    cout << "indeks where: " << distance(vec.begin(), where) << endl;

    cout << "vec po remove   : ";
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    vec.erase(where, vec.end());

    cout << "vec po remove   : ";
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
}
