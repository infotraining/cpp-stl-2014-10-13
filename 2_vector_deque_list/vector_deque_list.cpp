#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>
#include <chrono>

using namespace std;

/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/

// szablony funkcji umożliwiające sortowanie kontenera
// TODO

template <typename Container>
void sort(Container& cont)
{
    sort(begin(cont), end(cont));
}

template <typename T>
void sort(list<T>& cont)
{
    cont.sort();
}

vector<int> get_data()
{
    vector<int> result;

    for(int i = 0; i < 100; ++i)
        result.push_back(rand());

    return result;
}

int main()
{
    const int n = 10'000'000;

    // utwórz kontener
    using Container = vector<int>;

    Container cont;
    cont.resize(n);

    {
        cout << "Preparing data... Filling container." << endl;

        auto t1 = chrono::high_resolution_clock::now();

        // wypełnij kontener danymi
        for(int i = 0; i < n; ++i)
            cont.push_back(rand());

        auto t2 =  chrono::high_resolution_clock::now();

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << "ms" << endl;
    }

    {
        cout << "\nStart sorting..." << endl;

        auto t1 = chrono::high_resolution_clock::now();

        // posortuj kontener
        sort(cont);

        auto t2 =  chrono::high_resolution_clock::now();

        cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << "ms" << endl;
    }

    vector<int> v = get_data();
}
